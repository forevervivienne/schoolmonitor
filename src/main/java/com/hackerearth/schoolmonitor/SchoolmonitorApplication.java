package com.hackerearth.schoolmonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolmonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolmonitorApplication.class, args);
	}

}

package model;

public class Subject {

    private String subjectName;
    private String subjectDescription;
    private Integer numberOfStudents;

    public String getSubjectDescription() {
        return subjectDescription;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public Integer getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(Integer numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public void setSubjectDescription(String subjectDescription) {
        this.subjectDescription = subjectDescription;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
